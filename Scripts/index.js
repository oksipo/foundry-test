placeStep = (image, x, y, gridDisposition, rotation, size) => {
    Tile.create({
        img: image,
        width: size || 48,
        height: size || 48,
        scale: 1,
        x: x + gridDisposition,
        y: y + gridDisposition,
        z: 370,
        rotation: rotation,
        hidden: false,
        locked: false
    });
}

findRotation = (point1, point2) => {
    var rotation = Math.atan((point2.y - point1.y) / (point2.x - point1.x)) * 180 / Math.PI;
    if (point2.x < point1.x) {
        rotation += 180;
    }
    return rotation;
}

const updateToken = (token, x, y) => {
    token.data.flags.footsteps.info.positions = [{ x, y }, null]
}

lerp = (x, y, a) => x * (1 - a) + y * a;

Hooks.on('updateToken', (scene, data, moved) => {
    var flags = data.flags;
    if (flags.footsteps && flags.footsteps.info) {
        var info = flags.footsteps.info;
        if ((moved.x || moved.y) && info.useFootsteps) {
            var token = canvas.tokens.placeables.filter(x => x.id === data._id)[0];

            var lastpoint = { x: data.x, y: data.y };
            var gridDisposition = scene.data.grid / 4;
            var previousPoint = info.positions[0];
            const grid = scene.data.grid;
            const gridType = scene.data.gridType;
            // while (Math.abs(previousPoint.x - lastpoint.x) >= grid / 4 || Math.abs(previousPoint.y - lastpoint.y) >= grid / 4) {
            //     var nextPoint = { x: previousPoint.x, y: previousPoint.y };
            //     if (previousPoint.x - lastpoint.x <= grid / 4 * (-1)) {
            //         if (gridType == 2) {
            //             nextPoint.x += grid * Math.sqrt(3) / 2;
            //         }
            //         else {
            //             nextPoint.x += scene.data.grid;
            //         }
            //     }
            //     if (previousPoint.x - lastpoint.x >= grid / 4) {
            //         if (gridType == 2) {
            //             nextPoint.x -= grid * Math.sqrt(3) / 2;
            //         }
            //         else {
            //             nextPoint.x -= scene.data.grid;
            //         }
            //     }
            //     if (previousPoint.y - lastpoint.y <= grid / 4 * (-1)) {
            //         if (gridType == 2) {
            //             nextPoint.y += grid * Math.sqrt(3) / 2;
            //         }
            //         else {
            //             nextPoint.y += scene.data.grid;
            //         }
            //     }
            //     if (previousPoint.y - lastpoint.y >= grid / 4) {
            //         if (gridType == 2) {
            //             nextPoint.y -= grid * Math.sqrt(3) / 2;
            //         }
            //         else {
            //             nextPoint.y -= scene.data.grid;
            //         }
            //     }
            //     if (gridType == 2) {
            //         gridDisposition *= -1;
            //     }
            //}

            var rotation = findRotation(previousPoint, lastpoint);
            placeStep(info.config.mainImage, previousPoint.x, previousPoint.y, gridDisposition, rotation, info.config.mainImageSize);
            previousPoint = lastpoint;
            updateToken(token, lastpoint.x, lastpoint.y);
        }
    }
});

